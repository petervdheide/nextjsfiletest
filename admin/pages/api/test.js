import sendMail from '../../utils/sendMail';

const handler = async (req, res) => {

  await sendMail('ruben', 'pete', 'nice message', { partialTemplateName: 'somePartial.html' });

  res.status(200).json({ message: 'this is great' });
}

export default handler;

