import Handlebars from 'handlebars';
import { readFileSync } from 'fs';
import path from 'path';

const emailsDir = path.resolve(process.cwd(), 'emails');

const sendMail = async (
  from,
  to,
  subject = 'Form submission',
  data = {},
  templateName = null,
) => {
  const emailFile = readFileSync(path.join(emailsDir, templateName || 'generic.html'), {
    encoding: 'utf8',
  });

  const emailTemplate = Handlebars.compile(emailFile);

  if (data.partialTemplateName) {
    const partialFile = readFileSync(path.join(emailsDir, data.partialTemplateName), {
      encoding: 'utf8',
    });
    Handlebars.registerPartial('partial', partialFile);
  }

  const message = {
    from,
    to,
    subject,
    html: emailTemplate({
      ...data,
    }),
  };

  console.log(message);

  return true;
};

export default sendMail;
